import 'package:flutter/material.dart';
import 'package:project1/common/helper.dart';
import 'package:project1/submenu/tambahunggahan.dart';

class Unggahan extends StatefulWidget {
  const Unggahan({Key key}) : super(key: key);

  @override
  _UnggahanState createState() => _UnggahanState();
}

class _UnggahanState extends State<Unggahan> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      floatingActionButton: Container(
        padding: EdgeInsets.fromLTRB(15, 0, 10, 20),
        child: FloatingActionButton(
          child: Icon(
            Icons.add_sharp,
          ),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => TambahUnggah()));
          },
          backgroundColor: Color(0xFF083663),
        ),
      ),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Color(0xFF083663),
        title: Container(
          padding: EdgeInsets.only(left: 40),
          child: Text(
            "Data Jenis Unggahan ",
            style: TextStyle(
              fontSize: 20,
              color: Colors.white,
              fontFamily: "Sans",
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(),
      ),
    );
  }
}
