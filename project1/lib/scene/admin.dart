import 'package:flutter/material.dart';
import 'package:project1/common/helper.dart';
import 'package:project1/submenu/tambahadmin.dart';

class Admin extends StatefulWidget {
  const Admin({Key key}) : super(key: key);

  @override
  _AdminState createState() => _AdminState();
}

class _AdminState extends State<Admin> {
  int selectedRadio, selectedRadio1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    selectedRadio = 0;
    selectedRadio1 = 0;
  }

  setSelectedRadio(int val) {
    setState(() {
      selectedRadio = val;
    });
  }

  setSelectedRadio1(int val) {
    setState(() {
      selectedRadio1 = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      floatingActionButton: Container(
        padding: EdgeInsets.fromLTRB(15, 0, 10, 20),
        child: FloatingActionButton(
          child: Icon(
            Icons.add_sharp,
          ),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => TambahAdmin()));
          },
          backgroundColor: Color(0xFF083663),
        ),
      ),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Color(0xFF083663),
        title: Container(
          padding: EdgeInsets.only(left: 40),
          child: Text(
            "Data Admin",
            style: TextStyle(
              fontSize: 20,
              color: Colors.white,
              fontFamily: "Sans",
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            Column(
              children: [
                Container(
                  color: Color(0xFF083663),
                  width: SizeConfig.ScreenWidth,
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        child: Text(
                          'NO',
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                              fontFamily: 'Sans',
                              fontWeight: FontWeight.w800),
                        ),
                      ),
                      Container(
                        child: Text(
                          'JENIS ADMIN',
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                              fontFamily: 'Sans',
                              fontWeight: FontWeight.w800),
                        ),
                      ),
                      Container(
                        child: Text(
                          'AKSI',
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                              fontFamily: 'Sans',
                              fontWeight: FontWeight.w800),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              children: [
                Container(
                  color: Color(0xFF083663),
                  width: SizeConfig.ScreenWidth,
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 50),
                        child: Text(
                          '1',
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                              fontFamily: 'Sans',
                              fontWeight: FontWeight.w800),
                        ),
                      ),
                      Container(
                        child: Text(
                          'Admin',
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                              fontFamily: 'Sans',
                              fontWeight: FontWeight.w800),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 20),
                        child: Row(
                          children: [
                            Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Radio(
                                activeColor: Colors.green,
                                value: 0,
                                groupValue: selectedRadio1,
                                onChanged: (val) {
                                  setSelectedRadio1(val);
                                },
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Radio(
                                activeColor: Colors.green,
                                value: 1,
                                groupValue: selectedRadio1,
                                onChanged: (val) {
                                  setSelectedRadio1(val);
                                },
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Container(
                  color: Color(0xFF083663),
                  width: SizeConfig.ScreenWidth,
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 50),
                        child: Text(
                          '2',
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                              fontFamily: 'Sans',
                              fontWeight: FontWeight.w800),
                        ),
                      ),
                      Container(
                        child: Text(
                          'Klien',
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                              fontFamily: 'Sans',
                              fontWeight: FontWeight.w800),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 20),
                        child: Row(
                          children: [
                            Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Radio(
                                activeColor: Colors.green,
                                value: 0,
                                groupValue: selectedRadio,
                                onChanged: (val) {
                                  setSelectedRadio(val);
                                },
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Radio(
                                activeColor: Colors.green,
                                value: 1,
                                groupValue: selectedRadio,
                                onChanged: (val) {
                                  setSelectedRadio(val);
                                },
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
