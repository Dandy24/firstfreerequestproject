import 'package:flutter/material.dart';
import 'package:from_css_color/from_css_color.dart';
import 'package:project1/common/helper.dart';
import 'package:project1/scene/admin.dart';
import 'package:project1/scene/jenis.dart';
import 'package:project1/scene/pengguna.dart';
import 'package:project1/scene/ras.dart';
import 'package:project1/scene/unggahan.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    Widget firstOptionButton() => SizedBox(
          width: SizeConfig.ScreenWidth,
          height: 50,
          child: TextButton(
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Pengguna()));
            },
            style: ButtonStyle(
                overlayColor:
                    MaterialStateProperty.all(Colors.green.withOpacity(0.2)),
                backgroundColor:
                    MaterialStateProperty.all(Colors.blueGrey[400])),
            child: Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.all(3),
                child: Text(
                  "Data Jenis Pengguna",
                  style: TextStyle(
                      fontFamily: "Sans", color: Colors.white, fontSize: 15),
                )),
          ),
        );

    Widget twoOptionButton() => SizedBox(
          width: SizeConfig.ScreenWidth,
          height: 50,
          child: TextButton(
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Admin()));
            },
            style: ButtonStyle(
                overlayColor:
                    MaterialStateProperty.all(Colors.green.withOpacity(0.2)),
                backgroundColor:
                    MaterialStateProperty.all(Colors.blueGrey[400])),
            child: Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.all(3),
                child: Text(
                  "Data Admin",
                  style: TextStyle(
                      fontFamily: "Sans", color: Colors.white, fontSize: 15),
                )),
          ),
        );

    Widget thirdOptionButton() => SizedBox(
          width: SizeConfig.ScreenWidth,
          height: 50,
          child: TextButton(
            onPressed: () {
              Navigator.push(
                  context, (MaterialPageRoute(builder: (context) => Jenis())));
            },
            style: ButtonStyle(
                overlayColor:
                    MaterialStateProperty.all(Colors.green.withOpacity(0.2)),
                backgroundColor:
                    MaterialStateProperty.all(Colors.blueGrey[400])),
            child: Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.all(3),
                child: Text(
                  "Data Jenis Hewan",
                  style: TextStyle(
                      fontFamily: "Sans", color: Colors.white, fontSize: 15),
                )),
          ),
        );

    Widget fourthOptionButton() => SizedBox(
          width: SizeConfig.ScreenWidth,
          height: 50,
          child: TextButton(
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Ras()));
            },
            style: ButtonStyle(
                overlayColor:
                    MaterialStateProperty.all(Colors.green.withOpacity(0.2)),
                backgroundColor:
                    MaterialStateProperty.all(Colors.blueGrey[400])),
            child: Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.all(3),
                child: Text(
                  "Data Ras Hewan",
                  style: TextStyle(
                      fontFamily: "Sans", color: Colors.white, fontSize: 15),
                )),
          ),
        );

    Widget fifthOptionButton() => SizedBox(
          width: SizeConfig.ScreenWidth,
          height: 50,
          child: TextButton(
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Unggahan()));
            },
            style: ButtonStyle(
                overlayColor:
                    MaterialStateProperty.all(Colors.green.withOpacity(0.2)),
                backgroundColor:
                    MaterialStateProperty.all(Colors.blueGrey[400])),
            child: Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.all(3),
                child: Text(
                  "Data Jenis Unggahan",
                  style: TextStyle(
                      fontFamily: "Sans", color: Colors.white, fontSize: 15),
                )),
          ),
        );

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Color(0xFF083663),
        actions: [
          FlatButton(
            onPressed: () {},
            child: Text(
              "Log Out",
              style: TextStyle(fontSize: 20, color: Colors.white),
            ),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
        ],
      ),
      body: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.all(40),
                height: 450,
                width: 400,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: Color(0xFF083663),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      alignment: Alignment.center,
                      child: Text(
                        "Kelola Data",
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                            fontFamily: "Sans"),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    firstOptionButton(),
                    SizedBox(
                      height: 5,
                    ),
                    twoOptionButton(),
                    SizedBox(
                      height: 5,
                    ),
                    thirdOptionButton(),
                    SizedBox(
                      height: 5,
                    ),
                    fourthOptionButton(),
                    SizedBox(
                      height: 5,
                    ),
                    fifthOptionButton(),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
