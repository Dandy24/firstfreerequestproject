import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:project1/common/helper.dart';

class TambahAdmin extends StatefulWidget {
  const TambahAdmin({Key key}) : super(key: key);

  @override
  _TambahAdminState createState() => _TambahAdminState();
}

class _TambahAdminState extends State<TambahAdmin> {
  final TextEditingController nama = new TextEditingController();
  final TextEditingController email = new TextEditingController();
  final TextEditingController username = new TextEditingController();
  final TextEditingController password = new TextEditingController();
  final TextEditingController password2 = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    Widget namaTextFormField() => TextFormField(
          style: TextStyle(color: Colors.white),
          controller: nama,
          maxLines: 1,
          textInputAction: TextInputAction.next,
          decoration: InputDecoration(
            isDense: true,
            isCollapsed: true,
            filled: true,
            fillColor: Color(0xFF083663),
            focusColor: Colors.white,
            contentPadding: EdgeInsets.fromLTRB(13, 13, 0, 13),
            hintText: 'INPUT NAMA JENIS PENGGUNA',
            hintStyle: TextStyle(
                color: Colors.white,
                fontSize: 15,
                fontFamily: 'Sans',
                fontWeight: FontWeight.w800),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.horizontal(),
              borderSide: BorderSide(),
            ),
            errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.horizontal(),
                borderSide: BorderSide(color: Colors.red, width: 2)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.horizontal(),
                borderSide: BorderSide(width: 2)),
          ),
        );

    Widget emailTextFormField() => TextFormField(
          style: TextStyle(color: Colors.white),
          controller: email,
          keyboardType: TextInputType.emailAddress,
          maxLines: 1,
          textInputAction: TextInputAction.next,
          decoration: InputDecoration(
            isDense: true,
            isCollapsed: true,
            filled: true,
            fillColor: Color(0xFF083663),
            focusColor: Colors.white,
            contentPadding: EdgeInsets.fromLTRB(13, 13, 0, 13),
            hintText: 'INPUT EMAIL ADMIN',
            hintStyle: TextStyle(
                color: Colors.white,
                fontSize: 15,
                fontFamily: 'Sans',
                fontWeight: FontWeight.w800),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.horizontal(),
              borderSide: BorderSide(),
            ),
            errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.horizontal(),
                borderSide: BorderSide(color: Colors.red, width: 2)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.horizontal(),
                borderSide: BorderSide(width: 2)),
          ),
        );

    Widget usernameTextFormField() => TextFormField(
          style: TextStyle(color: Colors.white),
          controller: username,
          maxLines: 1,
          textInputAction: TextInputAction.next,
          decoration: InputDecoration(
            isDense: true,
            isCollapsed: true,
            filled: true,
            fillColor: Color(0xFF083663),
            focusColor: Colors.white,
            contentPadding: EdgeInsets.fromLTRB(13, 13, 0, 13),
            hintText: 'INPUT NAMA ADMIN',
            hintStyle: TextStyle(
                color: Colors.white,
                fontSize: 15,
                fontFamily: 'Sans',
                fontWeight: FontWeight.w800),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.horizontal(),
              borderSide: BorderSide(),
            ),
            errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.horizontal(),
                borderSide: BorderSide(color: Colors.red, width: 2)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.horizontal(),
                borderSide: BorderSide(width: 2)),
          ),
        );

    Widget passwordTextFormField() => TextFormField(
          style: TextStyle(color: Colors.white),
          controller: nama,
          maxLines: 1,
          obscureText: true,
          textInputAction: TextInputAction.next,
          decoration: InputDecoration(
            isDense: true,
            isCollapsed: true,
            filled: true,
            fillColor: Color(0xFF083663),
            focusColor: Colors.white,
            contentPadding: EdgeInsets.fromLTRB(13, 13, 0, 13),
            hintText: 'INPUT PASSWORD ADMIN',
            hintStyle: TextStyle(
                color: Colors.white,
                fontSize: 15,
                fontFamily: 'Sans',
                fontWeight: FontWeight.w800),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.horizontal(),
              borderSide: BorderSide(),
            ),
            errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.horizontal(),
                borderSide: BorderSide(color: Colors.red, width: 2)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.horizontal(),
                borderSide: BorderSide(width: 2)),
          ),
        );

    Widget password2TextFormField() => TextFormField(
          style: TextStyle(color: Colors.white),
          controller: password2,
          maxLines: 1,
          obscureText: true,
          textInputAction: TextInputAction.next,
          decoration: InputDecoration(
            isDense: true,
            isCollapsed: true,
            filled: true,
            fillColor: Color(0xFF083663),
            focusColor: Colors.white,
            contentPadding: EdgeInsets.fromLTRB(13, 13, 0, 13),
            hintText: 'ULANGI INPUT PASSWORD ADMIN',
            hintStyle: TextStyle(
                color: Colors.white,
                fontSize: 15,
                fontFamily: 'Sans',
                fontWeight: FontWeight.w800),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.horizontal(),
              borderSide: BorderSide(),
            ),
            errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.horizontal(),
                borderSide: BorderSide(color: Colors.red, width: 2)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.horizontal(),
                borderSide: BorderSide(width: 2)),
          ),
        );

    Widget saveButton() => SizedBox(
          width: 120,
          height: 50,
          child: TextButton(
            onPressed: () {},
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Color(0xFF083663))),
            child: Text(
              "SIMPAN",
              style: TextStyle(
                fontFamily: 'Sans',
                fontSize: 15,
                fontWeight: FontWeight.w700,
                color: Colors.white,
              ),
            ),
          ),
        );

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Color(0xFF083663),
        title: Container(
          child: Text(
            "Tambah Data Jenis Pengguna",
            style: TextStyle(
              fontSize: 20,
              color: Colors.white,
              fontFamily: "Sans",
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: SizeConfig.ScreenHeight,
          width: SizeConfig.ScreenWidth,
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: 10,
              ),
              Container(
                margin: EdgeInsets.only(left: 5),
                alignment: Alignment.centerLeft,
                child: Text(
                  "NAMA ADMIN",
                  style: TextStyle(
                      fontFamily: 'Sans',
                      fontSize: 15,
                      fontWeight: FontWeight.w800),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              namaTextFormField(),
              SizedBox(
                height: 10,
              ),
              Container(
                margin: EdgeInsets.only(left: 5),
                alignment: Alignment.centerLeft,
                child: Text(
                  "EMAIL ADMIN",
                  style: TextStyle(
                      fontFamily: 'Sans',
                      fontSize: 15,
                      fontWeight: FontWeight.w800),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              emailTextFormField(),
              SizedBox(
                height: 10,
              ),
              Container(
                margin: EdgeInsets.only(left: 5),
                alignment: Alignment.centerLeft,
                child: Text(
                  "USERNAME ADMIN",
                  style: TextStyle(
                      fontFamily: 'Sans',
                      fontSize: 15,
                      fontWeight: FontWeight.w800),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              usernameTextFormField(),
              SizedBox(
                height: 10,
              ),
              Container(
                margin: EdgeInsets.only(left: 5),
                alignment: Alignment.centerLeft,
                child: Text(
                  "PASSWORD",
                  style: TextStyle(
                      fontFamily: 'Sans',
                      fontSize: 15,
                      fontWeight: FontWeight.w800),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              passwordTextFormField(),
              SizedBox(height: 10),
              Container(
                margin: EdgeInsets.only(left: 5),
                alignment: Alignment.centerLeft,
                child: Text(
                  "ULANGI PASSWORD",
                  style: TextStyle(
                      fontFamily: 'Sans',
                      fontSize: 15,
                      fontWeight: FontWeight.w800),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              password2TextFormField(),
              SizedBox(
                height: 40,
              ),
              Container(alignment: Alignment.centerRight, child: saveButton()),
            ],
          ),
        ),
      ),
    );
  }
}
