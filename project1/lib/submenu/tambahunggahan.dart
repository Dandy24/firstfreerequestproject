import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:project1/common/helper.dart';

class TambahUnggah extends StatefulWidget {
  const TambahUnggah({Key key}) : super(key: key);

  @override
  _TambahUnggahState createState() => _TambahUnggahState();
}

class _TambahUnggahState extends State<TambahUnggah> {
  final TextEditingController input = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    Widget inputTextFormField() => TextFormField(
          style: TextStyle(color: Colors.white),
          controller: input,
          maxLines: 1,
          textInputAction: TextInputAction.next,
          decoration: InputDecoration(
            isDense: true,
            isCollapsed: true,
            filled: true,
            fillColor: Color(0xFF083663),
            focusColor: Colors.white,
            contentPadding: EdgeInsets.fromLTRB(13, 13, 0, 13),
            hintText: 'INPUT NAMA JENIS UNGGAHAN',
            hintStyle: TextStyle(
                color: Colors.white,
                fontSize: 15,
                fontFamily: 'Sans',
                fontWeight: FontWeight.w800),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.horizontal(),
              borderSide: BorderSide(),
            ),
            errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.horizontal(),
                borderSide: BorderSide(color: Colors.red, width: 2)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.horizontal(),
                borderSide: BorderSide(width: 2)),
          ),
        );

    Widget saveButton() => SizedBox(
          width: 120,
          height: 50,
          child: TextButton(
            onPressed: () {},
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Color(0xFF083663))),
            child: Text(
              "SIMPAN",
              style: TextStyle(
                fontFamily: 'Sans',
                fontSize: 15,
                fontWeight: FontWeight.w700,
                color: Colors.white,
              ),
            ),
          ),
        );

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Color(0xFF083663),
        title: Container(
          child: Text(
            "Tambah Data Jenis Unggahan",
            style: TextStyle(
              fontSize: 20,
              color: Colors.white,
              fontFamily: "Sans",
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: SizeConfig.ScreenHeight,
          width: SizeConfig.ScreenWidth,
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: 5,
              ),
              Container(
                margin: EdgeInsets.only(left: 5),
                alignment: Alignment.centerLeft,
                child: Text(
                  "NAMA JENIS UNGGAHAN",
                  style: TextStyle(
                      fontFamily: 'Sans',
                      fontSize: 15,
                      fontWeight: FontWeight.w800),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              inputTextFormField(),
              SizedBox(
                height: 40,
              ),
              Container(alignment: Alignment.centerRight, child: saveButton()),
            ],
          ),
        ),
      ),
    );
  }
}
