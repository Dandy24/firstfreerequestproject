import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:project1/common/helper.dart';
import 'package:project1/scene/dashboard.dart';

class Login extends StatefulWidget {
  const Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final TextEditingController username = new TextEditingController();
  final TextEditingController password = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    Widget usernameTextFormField() => TextFormField(
          controller: username,
          maxLines: 1,
          textInputAction: TextInputAction.next,
          decoration: InputDecoration(
              fillColor: Colors.white,
              focusColor: Colors.white,
              filled: true,
              isDense: true,
              contentPadding: EdgeInsets.fromLTRB(13, 13, 0, 13),
              hintText: 'email/username',
              hintStyle: TextStyle(color: Colors.indigo, fontSize: 16),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(color: Colors.green),
              ),
              errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(color: Colors.red, width: 2)),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(color: Colors.white, width: 2))),
        );

    Widget passwordTextFormField() => TextFormField(
          controller: password,
          maxLines: 1,
          obscureText: true,
          textInputAction: TextInputAction.next,
          decoration: InputDecoration(
              fillColor: Colors.white,
              focusColor: Colors.white,
              filled: true,
              isDense: true,
              contentPadding: EdgeInsets.fromLTRB(13, 13, 0, 13),
              hintText: 'password',
              hintStyle: TextStyle(color: Colors.indigo, fontSize: 16),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(color: Colors.green),
              ),
              errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(color: Colors.red, width: 2)),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(color: Colors.white, width: 2))),
        );

    Widget loginButton() => SizedBox(
          height: 40,
          width: 150,
          child: TextButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Dashboard()));
            },
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(Colors.white),
            ),
            child: Text(
              "login",
              style: TextStyle(fontSize: 20, color: Colors.indigo),
            ),
          ),
        );

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Row(
            children: [
              Flexible(
                flex: 1,
                child: Container(
                  color: Colors.white,
                ),
              ),
            ],
          ),
          Column(
            children: [
              Flexible(
                flex: 1,
                child: Container(
                  margin: EdgeInsets.only(left: 30),
                  decoration: BoxDecoration(
                      color: Colors.grey.withOpacity(0.2),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(450),
                        bottomLeft: Radius.circular(100),
                      )),
                ),
              ),
              Flexible(
                flex: 1,
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.grey.withOpacity(0.2),
                      borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(60),
                          topLeft: Radius.circular(60))),
                ),
              ),
              Flexible(
                flex: 1,
                child: Container(
                  margin: EdgeInsets.only(right: 30),
                  decoration: BoxDecoration(
                      color: Colors.grey.withOpacity(0.2),
                      borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(450),
                          topRight: Radius.circular(100))),
                ),
              ),
            ],
          ),
          SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(20),
              height: SizeConfig.ScreenHeight,
              width: SizeConfig.ScreenWidth,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Column(
                        children: [
                          Container(
                            padding:
                                EdgeInsets.only(top: 250, left: 50, right: 15),
                            child: Text(
                              "login",
                              style: TextStyle(
                                  fontSize: 40,
                                  color: Colors.indigo,
                                  fontWeight: FontWeight.w700),
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            padding: EdgeInsets.only(top: 200),
                            child: Image.asset("assets/img/anjing.png"),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 200),
                            child: Image.asset("assets/img/kucing.png"),
                          ),
                        ],
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                          padding: EdgeInsets.only(left: 40, right: 40),
                          child: usernameTextFormField()),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 40, right: 40),
                        child: passwordTextFormField(),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        alignment: Alignment.centerRight,
                        padding: EdgeInsets.only(left: 40, right: 40),
                        child: loginButton(),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
